'use strict'

const Schema = use('Schema')

class MessageSchema extends Schema {
  up () {
    this.create('messages', (table) => {
      table.increments()
      table.timestamps()
      table.integer('user_id').unsigned()
      table.foreign('user_id').references('users.id')
      table.integer('thread_id').unsigned()
      table.foreign('thread_id').references('threads.id')
      table.text('body')
    })
  }

  down () {
    this.drop('messages')
  }
}

module.exports = MessageSchema
