'use strict'

const Schema = use('Schema')

class ThreadSchema extends Schema {
  up () {
    this.create('threads', (table) => {
      table.increments()
      table.timestamps()
      table.integer('user1_id').unsigned()
      table.foreign('user1_id').references('users.id')
      table.integer('user2_id').unsigned()
      table.foreign('user2_id').references('users.id')
    })
  }

  down () {
    this.drop('threads')
  }
}

module.exports = ThreadSchema
