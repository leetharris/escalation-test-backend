'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

const Route = use('Route')

Route.on('/').render('welcome')

Route
  .group(() => {
    Route.post('users', 'UserController.store').middleware([
      'auth'
    ]);
    Route.get('users/:id', 'UserController.show').middleware([
      'auth'
    ]);
    Route.get('users', 'UserController.index').middleware([
      'auth'
    ]);
    Route.post('login', 'UserController.login');
    Route.resource('threads', 'ThreadController')
      .only([
        'index'
      ])
      .middleware([
        'auth'
      ]);
    Route.resource('messages', 'MessageController')
      .only([
        'store',
        'index'
      ])
      .middleware([
        'auth'
      ]);
  })
  .prefix('api/v1')
