'use strict'

const Model = use('Model')

class Thread extends Model {
  static boot () {
    super.boot()

    this.addHook('afterFetch', async (data) => {
      for(let item of data) {
        // We invoke the User model.
        const User = use('App/Models/User');

        // We find the first user and return the username.
        let user1 = await User.query().where('id', item.user1_id).first();
        item.user1_name = user1.username;

        // And the same for the second user.
        let user2 = await User.query().where('id', item.user2_id).first();
        item.user2_name = user2.username;
      }
    });
  }

  messages () {
    return this.hasMany('App/Models/Message');
  }

  // We create a second relationship so that we can add query constraints later.
  latestMessage () {
    return this.hasMany('App/Models/Message', 'id', 'thread_id')
  }

  static get Serializer() {
    return 'JsonApi/Serializer/LucidSerializer'; // Override Lucid/VanillaSerializer
  }

}

module.exports = Thread
