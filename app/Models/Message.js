'use strict'

const Model = use('Model')

class Message extends Model {
  thread () {
    return this.belongsTo('App/Models/Thread')
  }

  user () {
    return this.belongsTo('App/Models/User')
  }

  static get Serializer() {
    return 'JsonApi/Serializer/LucidSerializer'; // Override Lucid/VanillaSerializer
  }
}

module.exports = Message
