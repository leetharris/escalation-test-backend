'use strict'

class UserController {
  /**
   * Logs a user in and returns an auth key.
   *
   * @param request
   * @param auth
   * @returns {Promise<{userID: *, auth: *}>}
   */
  async login ({ request, auth }) {
    const User = use('App/Models/User');
    const { email, password } = request.all();

    // We attempt to authorize the user with the provided login details
    let authDetails = await auth.attempt(email, password);

    // We pull the user details to include in the response.
    let authUser = await User.query().where('email', email).first();

    return {
      userID: authUser.id,
      auth: authDetails
    };
  }

  /**
   * Displays a user's information.
   *
   * @param auth
   * @param params
   * @returns {Promise<*>}
   */
  async show ({ auth, params }) {
    const User = use('App/Models/User');
    return User.query().where('id', params.id).first();
  }

  /**
   * Returns a list of all users.
   *
   * @param auth
   * @param params
   * @returns {Promise<*|Promise<Response>>}
   */
  async index ({auth, params}) {
    const User = use('App/Models/User');
    return User.query().fetch();
  }

  /**
   * Creates a new user.
   *
   * @param request
   * @param auth
   * @param params
   * @returns {Promise<*>}
   */
  async store ({request, auth, params}) {
    const User = use('App/Models/User');
    const user = new User();

    const { email, password, username } = request.all();

    user.email = email;
    user.password = password;
    user.username = username;

    await user.save();

    return user;
  }
}

module.exports = UserController
