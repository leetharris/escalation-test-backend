'use strict'

class MessageController {
  /**
   * Creates a new message.
   * @param request
   * @param auth
   * @param params
   * @returns {Promise<*>}
   */
  async store({request, auth, params}) {
    const ThreadModel = use('App/Models/Thread');

    const targetUserID  = request.input('data.relationships.targetUser.data.id');
    const userIDArray = [Number(targetUserID), auth.user.id];

    // We check to see if a thread exists between these two users.
    let existingThread = await ThreadModel
      .query()
      .whereIn('user1_id', userIDArray)
      .whereIn('user2_id', userIDArray)
      .fetch();

    const MessageModel = use('App/Models/Message');
    let newMessage = new MessageModel();
    newMessage.body = request.input('data.attributes.body');
    newMessage.user().associate(auth.user);

    // If the thread already exists, we attach that ID to the new message. If not, we create a new Thread.
    if(existingThread.size()) {
      newMessage.thread().associate(existingThread.first());
    } else {
      let newThread = new ThreadModel();
      newThread.user1_id = auth.user.id;
      newThread.user2_id = targetUserID;
      await newThread.save();
      newMessage.thread().associate(newThread);
    }

    await newMessage.save();

    return newMessage;
  }

  /**
   * Returns a list of all Messages. Can be filtered by thread_id.
   *
   * @param request
   * @param auth
   * @param params
   * @returns {Promise<*>}
   */
  async index({request, auth, params}) {
    const MessageModel = use('App/Models/Message');

    let allMessages = null;

    // If we have a filter query param, we only return messages from that thread.
    if(request.get().filter && request.get().filter.thread_id) {
      allMessages = await MessageModel.query().where('thread_id', request.get().filter.thread_id).fetch();
    } else {
      allMessages = await MessageModel.all();
    }

    return allMessages;
  }
}

module.exports = MessageController
