'use strict'

class ThreadController {
  /**
   * Returns a list of all threads belonging to the requesting user.
   *
   * @param request
   * @param auth
   * @returns {Promise<*>}
   */
  async index({request, auth}) {
    const ThreadModel = use('App/Models/Thread');

    return await ThreadModel
      .query()
      .where('user1_id', auth.user.id)
      .orWhere('user2_id', auth.user.id)
      .fetch();
  }
}

module.exports = ThreadController
