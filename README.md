# Escalation Test - Backend Chat API

## Description

**API Spec Used** : JSONAPI (find out more [here](http://jsonapi.org/))

**Tech Stack** : AdonisJS + Node.js, MySQL

**How To Setup Environment**

1. Clone repo and run `npm install`.
2. Install the AdonisJS CLI using `npm i -g @adonisjs/cli`.
3. Set up an `.env` file using the `.env.example` for a sample format.
4. Run all SQL migrations using `adonis migration:run`.
5. Launch a dev server using `adonis serve --dev`.

## Documentation

All endpoints are documented using API Blueprint in Apiary. You can find all documentation at [this](https://escalationtest.docs.apiary.io/) link.

## TODO

This is a list of luxury items that will be completed eventually to make this a production-ready app:

- Add validation to request inputs and add more JSONAPI middleware to check for proper headers / format.
- Use Pusher to set up a websocket broadcast to transmit chat API updates directly to the frontend. 
- Enforce JSONAPI across all aspects of the app and use pure REST for login/sessions.
