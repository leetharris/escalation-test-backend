module.exports = {
  "globalOptions": {
    "convertCase": "snake_case",
    "unconvertCase": "camelCase"
  },
  // Register JSON API Types here..
  "registry": {
    "user": {
      "model": 'App/Models/User',
      "structure": {
        "links": {
          self: (data) => {
            return '/users/' + data.id
          }
        },
        "topLevelLinks": {
          self: '/users'
        }
      }
    },

    "message": {
      "model": 'App/Models/Message',
      "structure": {
        "links": {
          self: (data) => {
            return '/messages/' + data.id
          }
        },
        "topLevelLinks": {
          self: '/messages'
        }
      }
    },

    "thread": {
      "model": 'App/Models/Thread',
      "structure": {
        "links": {
          self: (data) => {
            return '/threads/' + data.id
          }
        },
        "topLevelLinks": {
          self: '/threads'
        }
      }
    }
  }
};
